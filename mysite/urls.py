
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^',include('back.urls')),
    url(r'^index1/',include('back.urls')),
    url(r'^patientsub/',include('back.urls')),
    url(r'^doctorsub/',include('back.urls')),
    url(r'^patientl_list/',include('back.urls')),
    url(r'^doctorl_list/',include('back.urls')),
    url(r'^patient_table/',include('back.urls')),
    url(r'^doctorselect/',include('back.urls')),
    url(r'^appointmentcrt/',include('back.urls')),
    # url(r'^doctor_table/',include('back.urls')),

    #url(r'^patientl_list/?email=/',include('back.urls')),


]
