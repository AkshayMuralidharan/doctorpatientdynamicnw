from __future__ import unicode_literals

from django.apps import AppConfig
from django.forms import patientForm
from django.forms import doctorForm



class BackConfig(AppConfig):
    name = 'back'
