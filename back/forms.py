from django import forms
from models import patient
from models import doctor
from models import appointment

from django.forms import ModelForm

class patientForm(forms.ModelForm):
    class Meta:
        model = patient
        app_label = 'back'
        db_table = 'back_patient'
        fields = ('name', 'email', 'phone')


class doctorForm(forms.ModelForm):
    class Meta:
        model = doctor
        app_label = 'back'
        db_table = 'back_doctor'
        fields = ('name', 'designation')

class appointmentForm(forms.ModelForm):
    class Meta:
        model = appointment
        app_label = 'back'
        db_table = 'back_doctor'
        fields = ('patientname', 'dctr')
