from django.shortcuts import render, redirect, get_object_or_404
import psycopg2
from django.shortcuts import render_to_response
from django.db import models
import json
from django.core import serializers
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import sys
import logging
import urlparse
import psycopg2 as dbapi2
from back.models import patient
from back.models import doctor
from back.models import appointment
from django.http import JsonResponse
logger = logging.getLogger(__name__)

#from django.template.context import RequestContext
from .forms import patientForm
from .forms import doctorForm
from .forms import appointmentForm

#disabling csrf (cross site request forgery)
@csrf_exempt
def patientall(request):



        template = loader.get_template('index01.html')

        # return HttpResponse(json.dumps(context), content_type="application/json")

        return HttpResponse(template.render())


def doctorselect(request):
    data = []

    data['doctort'] = doctor.objects.all()
    data['patientt'] = patient.objects.all()
    # template = loader.get_template('appoinment_create.html')
    return HttpResponse(json.dumps(data), content_type="application/json")

@csrf_exempt
def patientin(request):


    if request.method == 'POST':
        if request.is_ajax():




            #getting values from post
            name = request.POST.get('name')
            email = request.POST.get('email')
            phone = request.POST.get('phone')


            form = patientForm(request.POST or None)
            if form.is_valid():

                f = form.save()

            # context = {
            #       'name': name,
            #       'email': email,
            #       'phone': phone,
            #       'status': "sucess",
            #
            #     }
            data = {"name":name, "email":email, "phone":phone}
            return JsonResponse(data)


        # template = loader.get_template('index01.html')
        # # return HttpResponse(json.dumps(context), content_type="application/json")
        return render(request, 'index01.html')
        # return HttpResponse(template.render())
    else:
        #if post request is not true
        #returing the form template
        template = loader.get_template('index01.html')
        return HttpResponse(template.render())


def patientl_list(request):
    print 'test'
    print request
    currentUrl = request.get_full_path()
    parsed = urlparse.urlparse(currentUrl)
    urlSplit =  currentUrl.split('?')
    if len(urlSplit) > 1:
        email = urlparse.parse_qs(parsed.query)['email']
        # if mail:
        print email[0]


        patiente = patient.objects.filter(email = email[0])
        print patiente
        data = []
        i = 0
        for obj in patiente:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "email": obj.email,
                "phone": obj.phone,
            }
            i += 1
        return HttpResponse(json.dumps(data), content_type="application/json")

    else:

        patientl = patient.objects.all()
        data = []

        # logger.error('tada')

        i = 0
        for obj in patientl:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "email": obj.email,
                "phone": obj.phone,

            }
            i += 1
        print data

        # return render(request, template_name, data)
        # template = loader.get_template('patientl_list.html')
        # return HttpResponse(template.render(context, request))
        # return render_to_response('patientl_list.html', {'obj': models.patient.objects.all()})
        # response_data = {}
        # response_data['result'] = data
        return HttpResponse(json.dumps(data), content_type="application/json")

def patient_table(request):
    appointmentl = appointment.objects.all()
    data = []

    # logger.error('tada')

    i = 0
    for obj in appointmentl:
        data.append([])
        data[i] = {
            "id": obj.id,
            "patientname": obj.patientname,
            "dctr": obj.dctr,
            # "date": obj.date
        }
        i += 1
    return HttpResponse(json.dumps(data), content_type="application/json")
    # context = dict()
    #
    # context['appoimnt'] = appointment.objects.all()
    #
    # template = loader.get_template('patient_table.html')
    # return HttpResponse(template.render(context, request))
    # return render(request, context)
   # return render_to_response('patient_table.html', {'obj': models.patient.objects.all()})

# def doctor_table(request):
#     context = dict()
#
#     context['doctort'] = doctor.objects.all()
#     template = loader.get_template('doctor_table.html')
#     return HttpResponse(template.render(context, request))

   # return render_to_response('doctor_table.html', {'obj': models.doctor.objects.all()})

@csrf_exempt
def doctorin(request):
    #if post request came
    if request.method == 'POST':
        if request.is_ajax():


            #getting values from post
           name = request.POST.get('name')
           designation = request.POST.get('designation')
           form = doctorForm(request.POST or None)
           if form.is_valid():
              #f = doctor(name='name', designation='designation')
              f = form.save()



           data1 = {"name":name, "designation":designation}
           return JsonResponse(data1)

            # template = loader.get_template('showdata.html')
        #   return HttpResponse(json.dumps(context), content_type="application/json")

        return render(request, 'index01.html')



    else:
        #if post request is not true
        #returing the form template
        template = loader.get_template('index01.html')
        return HttpResponse(template.render())


def doctorl_list(request):
    print 'test'
    print request
    currentUrl = request.get_full_path()
    parsed = urlparse.urlparse(currentUrl)
    urlSplit =  currentUrl.split('?')
    if len(urlSplit) > 1:
        name = urlparse.parse_qs(parsed.query)['name']
        # if mail:
        print name[0]


        doctorn = doctor.objects.filter(name = name[0])
        print doctorn
        data = []
        i = 0
        for obj in doctorn:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "designation": obj.designation
            }
            i += 1
        return HttpResponse(json.dumps(data), content_type="application/json")

    else:

        doctorl = doctor.objects.all()
        data = []

        # logger.error('tada')

        i = 0
        for obj in doctorl:
            data.append([])
            data[i] = {
                "id": obj.id,
                "name": obj.name,
                "designation": obj.designation
            }
            i += 1
        print data

        #return render(request, template_name, data)
        #template = loader.get_template('patientl_list.html')

        # response_data = {}
        # response_data['result'] = data
        return HttpResponse(json.dumps(data), content_type="application/json")

@csrf_exempt
def appointmentcrt(request):


    if request.method == 'POST':
        if request.is_ajax():


        #getting values from post
            patientname = request.POST.get('patientname')
            dctr = request.POST.get('dctr')

            form = appointmentForm(request.POST or None)
            if form.is_valid():

                f = form.save()

            # context = {
            #       'patientname': patientname,
            #       'dctr': dctr,
            #
            #       'status': "sucess",
            #
            #     }
            data2 = {"patientname":patientname, "dctr":dctr}
            return JsonResponse(data2)
        return render(request, 'index01.html')
        # template = loader.get_template('appoinment_create.html')
        # # return HttpResponse(json.dumps(context), content_type="application/json")
        #
        # return HttpResponse(template.render())
    else:
        #if post request is not true
        #returing the form template
        template = loader.get_template('appoinment_create.html')
        return HttpResponse(template.render())
