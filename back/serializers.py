from rest_framework import serializers
from .models import patient
from .models import doctor


class patientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = patient
        fields = ('name', 'email', 'phone')

class doctorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = doctor
        fields = ('name', 'designation')
