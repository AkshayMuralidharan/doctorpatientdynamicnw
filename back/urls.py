from django.conf.urls import url, include


from django.contrib import admin


#importing views
#we need to create views.py
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #define the url getdata that we have written inside form
    url(r'^patientsub/', views.patientin),
    url(r'^patientl_list/', views.patientl_list),
    url(r'^doctorl_list/', views.doctorl_list),
    url(r'^patient_table/', views.patient_table),
    url(r'^doctorselect/', views.doctorselect),
    url(r'^appointmentcrt/', views.appointmentcrt),
    # url(r'^doctor_table/', views.doctor_table),
    #url(r'^patientl_list/?email=/', views.patientl_list1),

    url(r'^doctorsub/', views.doctorin),
    #defining the view for root URL
    url(r'^', views.patientall),
    url(r'^index1/', views.patientin),

]
